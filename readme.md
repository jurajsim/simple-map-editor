## Demo

Will be here: https://jurajsim.gitlab.io/simple-map-editor/  
(But currently kinda broken...)

## TODO

- use @material-ui for UI, do not mix it with model & leaflet map
- prepare web-component version to be able to use it in vueJs
- finish features
  - marker settings (colors, content/description)
  - path settings (colors, pattern, content/description)
  - polygon drawing (colors, pattern, content/description) 
  - freehand drawing (notes)
  - gps recording for making paths on mobile device
  - make it usable on smaller screens (keep material design)

## Sources

- react boilreplate: https://github.com/HashemKhalifa/webpack-react-boilerplate 
