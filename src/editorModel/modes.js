import OBJECT_TYPES from './objects/types';

export default class {
    static get MOVE_OR_ROTATE_MODE() {
        return 'move_or_rotate_mode';
    }

    static get EDIT_MODE() {
        return 'edit_mode';
    }

    static get DRAW_MODE() {
        return 'draw_mode';
    }

    static isMoveOrRotateMode(editormodel) {
        return editormodel.mode === this.MOVE_OR_ROTATE_MODE;
    }

    static isEditMode(editormodel) {
        return editormodel.mode === this.EDIT_MODE;
    }

    static isDrawingMarker(editormodel) {
        return (
            editormodel.mode === this.DRAW_MODE && editormodel.drawModeType === OBJECT_TYPES.MARKER
        );
    }

    static isDrawingLine(editormodel) {
        return (
            editormodel.mode === this.DRAW_MODE && editormodel.drawModeType === OBJECT_TYPES.LINE
        );
    }
}
