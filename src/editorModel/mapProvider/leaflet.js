import L from 'leaflet';
import { Marker as MapMarker, MarkerIcon } from '../../lib/editor/map/mapMarker';

export default class LeafletProvider {
    constructor(map) {
        this.map = map;
    }

    addObjectToLayer(object, targetLayer) {
        console.log('add object to layer', object, targetLayer);
        targetLayer.addLayer(object);
    }

    stopEventPropagation(evt) {
        L.DomEvent.stopPropagation(evt);
    }

    addLayerToMap(layer) {
        console.log('Adding layer ', layer, ' to ', this.map);
        this.map.addLayer(layer);
    }

    createDrawLayer() {
        return new L.FeatureGroup();
    }

    fitBounds(bounds) {
        this.map.fitBounds(bounds);
    }

    setCenter(lat, lng, zoom) {
        this.map.setView([lat, lng], zoom);
    }

    setZoom(zoom) {
        this.map.setZoom(zoom);
    }

    zoomIn() {
        this.map.zoomIn();
    }

    zoomOut() {
        this.map.zoomOut();
    }

    createMarker(lat, lng, title) {
        const mmm = new MapMarker(L.latLng(lat, lng), {
            icon: MarkerIcon,
            title,
            // ??
            // zIndexOffset: this.options.zIndexOffset,
            transform: true,
            draggable: true,
        });
        console.log('new map marker created', mmm);
        return mmm;
    }
}
