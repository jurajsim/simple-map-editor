import EVENT_TYPES from '../eventTypes';
import types from './types';
import BaseObject from './base';
import L from 'leaflet';

export default class Marker extends BaseObject {
    constructor(options) {
        super(options);
        this.type = types.MARKER;
        this.layer = options.layer;
        this.originalLatLng;
        this.$setLayerObject(
            options.layerObject ||
                this.mapProvider.createMarker(options.meta.lat, options.meta.lng),
        );
        this.meta = options.meta;
        this.draggable = false;
    }

    setOriginalLatLng() {
        this.originalLatLng = this.getLatLng();
    }

    clearOriginalLatLng() {
        this.originalLatLng = null;
    }

    enableDrag(onDragStartedHandler, onDragEndedHandler, onDraggingHandler, dragMarker) {
        this.onDragStartedHandler = (evt) => {
            onDragStartedHandler(evt, dragMarker);
        };
        this.onDragEndedHandler = (evt) => {
            onDragEndedHandler(evt, dragMarker);
        };
        this.onDraggingHandler = (evt) => {
            onDraggingHandler(evt, dragMarker);
        };
        this.layerObject.on('dragstart', this.onDragStartedHandler);
        this.layerObject.on('dragend', this.onDragEndedHandler);
        this.layerObject.on('drag', this.onDraggingHandler);
        this.layerObject.dragging.enable();
        this.draggable = true;
    }

    disableDrag() {
        this.layerObject.dragging.disable();
        if (this.onDragEndedHandler) {
            this.layerObject.off('dragend', this.onDragEndedHandler);
        }
        if (this.onDraggingHandler) {
            this.layerObject.off('drag', this.onDraggingHandler);
        }
        if (this.onDragStartedHandler) {
            this.layerObject.off('dragstart', this.onDragStartedHandler);
        }
    }

    getTitle() {
        return this.meta.name;
    }

    setTitle(title) {
        this.meta.name = title;
    }

    getLatLng() {
        return this.layerObject.getLatLng();
    }

    moveToLatLng(lat, lng) {
        this.layerObject.setLatLng(L.latLng(lat, lng));
    }

    select() {
        this.layerObject.getElement().classList.add('marker-selected'); // style.outline = '2px solid #1d2b5fa1';
        super.select();
    }

    hide() {
        if (!this.visible) {
            return;
        }
        super.hide();
        this.emit(EVENT_TYPES.MARKER.VISIBILITY_CHANGED, this.visible);
    }

    show() {
        if (this.visible) {
            return;
        }
        super.show();
        this.emit(EVENT_TYPES.MARKER.VISIBILITY_CHANGED, this.visible);
    }

    unselect() {
        // this.markerObject.getElement().style.outline = 'none';
        this.layerObject.getElement().classList.remove('marker-selected');
        super.unselect();
    }

    toJSON() {
        console.log('marker to json...');
        let latLng = this.layerObject.getLatLng();
        return {
            id: this.id,
            type: this.type,
            meta: {
                lat: latLng.lat,
                lng: latLng.lng,
                name: this.meta.name,
            },
        };
    }
}
