import debug from 'debug';
import types from './types';
import BaseObject from './base';

const log = debug('polyline');

export default class Path extends BaseObject {
    constructor(options) {
        super(options);
        this.type = types.LINE;
        this.layer = options.layer;
        this.$setLayerObject(options.polyline);
        this.meta = options.meta;
        this.draggable = false;
        this.rotateOrResize = false;
    }

    disableTransform() {
        this.polylineObject.transform.disable();
        this.rotateOrResize = false;
    }

    enableTransform() {
        this.polylineObject.transform.enable();
        this.rotateOrResize = true;
    }

    enableDrag() {
        this.polylineObject.dragging.enable();
        this.draggable = true;
        log('dragging enabled');
    }

    disableDrag() {
        this.polylineObject.dragging.disable();
    }

    unselect() {
        if (this.rotateOrResize) {
            this.disableTransform();
        }
        super.unselect();
    }

    toJSON() {
        let latLngs = [];
        if (this.polylineObject) {
            latLngs = this.polylineObject.getLatLngs().map((latLng) => {
                return {
                    lat: latLng.lat,
                    lng: latLng.lng,
                };
            });
        }
        return {
            id: this.id,
            type: this.type,
            //meta: _.omit(this.meta, ['map']),
            latLngs,
        };
    }
}
