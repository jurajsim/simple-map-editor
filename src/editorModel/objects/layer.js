import types from './types';
import BaseObject from './base';
import EVENT_TYPES from '../eventTypes';

export default class Layer extends BaseObject {
    constructor(options) {
        super(options);
        this.type = types.LAYER;
        this.$setLayerObject(this.mapProvider.createDrawLayer());
        this.meta = options.meta;
        this.children = [];
    }

    getName() {
        return this.meta.name;
    }

    add(obj, notify = true) {
        this.children.push(obj);
        this.mapProvider.addObjectToLayer(obj.layerObject, this.layerObject);
        if (notify) {
            this.emit(EVENT_TYPES.LAYER.CHILDREN_ADDED, this.children);
        }
    }

    hide() {
        if (!this.visible) {
            return;
        }
        super.hide();
        // for (let i = 0; i < this.children.length; i++) {
        //     this.children[i].hide();
        // }
        this.emit(EVENT_TYPES.LAYER.VISIBILITY_CHANGED, this.visible);
    }

    show() {
        if (this.visible) {
            return;
        }
        super.show();
        // for (let i = 0; i < this.children.length; i++) {
        //     this.children[i].show();
        // }
        this.emit(EVENT_TYPES.LAYER.VISIBILITY_CHANGED, this.visible);
    }

    toJSON() {
        console.log('LAYER', this.meta.name);
        const out = {
            type: this.type,
            meta: {
                name: this.getName(),
            },
            id: this.id,
            children: this.children.map((child) => child.toJSON()),
        };
        return out;
    }
}
