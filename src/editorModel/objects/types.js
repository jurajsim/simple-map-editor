export default {
    MARKER: 'marker',
    LINE: 'polyline',
    LAYER: 'layer',
};
