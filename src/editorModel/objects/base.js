import uuid from 'uuid/v4';
import EventEmitter from '../../lib/util/eventEmitter';

export default class BaseObject extends EventEmitter {
    constructor(options) {
        super();
        this.mapProvider = options.mapProvider;
        this.map = this.mapProvider.map;
        this.id = options.id || uuid();
        this.visible = true;
        this.selected = false;
        this.layerObject = null;
    }

    $setLayerObject(object) {
        this.layerObject = object;
    }

    unselect() {
        this.selected = false;
        if (this.draggable) {
            this.disableDrag();
        }
    }

    select() {
        this.selected = true;
    }

    hide() {
        if (this.selected) {
            this.unselect();
        }
        if (this.layerObject) {
            this.map.removeLayer(this.layerObject);
        }
        this.visible = false;
    }

    show() {
        if (this.layerObject) {
            this.map.addLayer(this.layerObject);
        }
        this.visible = true;
    }

    toJSON() {
        throw new Error('not implemented');
    }
}
