import debug from 'debug';
import uuid from 'uuid';
import L from 'leaflet';
import 'leaflet-gpx';
import LeafletProvider from './mapProvider/leaflet';
import MODES from './modes';
import EventEmitter from '../lib/util/eventEmitter';
import EVENT_TYPES from './eventTypes';
import EDITOR_EVENT_TYPES from '@/lib/editor/eventTypes';
import Layer from './objects/layer';
import Marker from './objects/marker';
import Path from './objects/path';
import TYPES from './objects/types';

const log = debug('editorModel');

const DEFAULT_ZOOM = 13;
const DEFAULT_LAT = 50.08804;
const DEFAULT_LNG = 14.42076;

class EditorModel extends EventEmitter {
    constructor(data) {
        super();
        this.mapProvider = null;
        this.$layerCounter = 1;
        this.$markerCounter = 1;
        this.$pathCounter = 1;
        this.state = Object.freeze({
            layers: [],
            mode: MODES.MOVE_OR_ROTATE_MODE,
            drawModeType: null,
            selectedLayer: null,
            selectedLayerId: null,
            selection: [],
            selectionIds: [],
            minZoom: 5,
            maxZoom: 25,
            lat: DEFAULT_LAT,
            lng: DEFAULT_LNG,
            zoom: DEFAULT_ZOOM,
            rightDrawer: {
                opened: false,
            },
        });
        this.$temporaryData = data;
        // this.restoreState();
    }

    get rightDrawer() {
        return this.state.rightDrawer;
    }

    get lat() {
        return this.state.lat;
    }

    get lng() {
        return this.state.lng;
    }

    get minZoom() {
        return this.state.minZoom;
    }

    get maxZoom() {
        return this.state.maxZoom;
    }

    get zoom() {
        return this.state.zoom;
    }

    get mode() {
        return this.state.mode;
    }

    get layers() {
        return this.state.layers;
    }

    get selection() {
        return this.state.selection;
    }

    get drawModeType() {
        return this.state.drawModeType;
    }

    get selectedLayer() {
        return this.state.selectedLayer;
    }

    setLatLng(lat, lng, updateState = true) {
        if (updateState) {
            this.setState({
                lat,
                lng,
            });
        }
        this.emit(EVENT_TYPES.MAP.LAT_LNG_CHANGE, { lat, lng });
    }

    $restoreState() {
        if (!this.$temporaryData) {
            return;
        }
        const newState = this.setMode(
            this.$temporaryData.mode,
            {
                drawModeType: this.$temporaryData.drawModeType,
            },
            false,
        );

        const zoom = this.$temporaryData.zoom || DEFAULT_ZOOM;
        const lat = this.$temporaryData.lat || DEFAULT_LAT;
        const lng = this.$temporaryData.lng || DEFAULT_LNG;

        this.setZoom(zoom, false);
        this.setLatLng(lat, lng, false);
        this.mapProvider.setCenter(lat, lng, zoom);
        this.setState({
            zoom,
            ...newState,
            lat,
            lng,
        });
    }

    $recreateLayersFromData(data) {
        log('Recreating layers from data', data);
        const layers = [];
        for (let i = 0; i < data.length; i++) {
            const layer = this.$createLayer(data[i]);
            layers.push(layer);
            log(`Got ${data[i].children.length} children`);
            if (data[i].children && data[i].children.length > 0) {
                for (let j = 0; j < data[i].children.length; j++) {
                    const child = data[i].children[j];
                    log('Got child', child);
                    if (child.type === TYPES.MARKER) {
                        log('Got marker meta', child.meta);
                        this.createAndAddMarker(
                            this.mapProvider.createMarker(
                                child.meta.lat,
                                child.meta.lng,
                                child.meta.name,
                            ),
                            layer,
                            child.meta,
                            false,
                        );
                        // let marker = this.$createMarker(this.mapProvider.createMarker(child.meta.lat, child.meta.lng), layer, {
                        //     meta: child.meta
                        // });
                        // log('new marker', marker);
                        // layer.add(marker, false);
                    }
                }
            }
            this.$addLayer(layer, false);
        }
        this.setState({
            layers,
        });
        // for (let i = 0; i < layers.length; i++) {
        //     this.mapProvider.addLayerToMap(layers[i].layerObject);
        //     for (let j = 0; j < layers[i].children.length; j++) {
        //         debug('Got child', layers[i].children[j].mapLayer);
        //         layers[i].layerObject.addLayer(layers[i].children[j].layerObject);
        //     }
        // }
        this.emit(EVENT_TYPES.LAYERS.CHANGED, this.state.layers);
    }

    syncWithMap = (map) => {
        // TODO: pick provider automatically
        if (!this.mapProvider) {
            this.mapProvider = new LeafletProvider(map);
        }
        this.emit(EDITOR_EVENT_TYPES.MAP.READY, { mapProvider: this.mapProvider });

        if (
            this.$temporaryData &&
            this.$temporaryData.layers &&
            this.$temporaryData.layers.length > 0
        ) {
            this.$recreateLayersFromData(this.$temporaryData.layers);
            log('TEMP DATA', this.$temporaryData);
            if (this.$temporaryData.selectedLayerId) {
                this.selectLayerById(this.$temporaryData.selectedLayerId);
                this.selectLayerById(this.layers[0].id);
            }
        } else {
            // default layer
            debug('No layers. Creating default one...');
            const defaultLayer = this.createAndAddLayer();
            debug('No layer selected. Selecting default one...');
            this.selectLayer(defaultLayer);
        }
        this.$restoreState();
    };

    setZoom(zoom, updateState = true) {
        if (updateState) {
            this.setState({
                zoom,
            });
        }

        this.emit(EVENT_TYPES.MAP.ZOOM_CHANGE, zoom);
    }

    clearSelection() {
        log('Clearing selection');
        if (this.state.selectionIds.length === 0) {
            return;
        }
        for (let i = 0; i < this.state.selection.length; i++) {
            log('Unselecting object ', i);
            this.state.selection[i].unselect();
        }
        this.setState({
            selection: [],
            selectionIds: [],
        });
        this.emit(EVENT_TYPES.OBJECTS_SELECTION.CHANGED, this.state.selection);
    }

    removeFromSelection(object) {
        if (!(object instanceof Marker)) {
            return;
        }
        if (this.state.selectionIds.indexOf(object.id) === -1) {
            return;
        }
        const selectionIdx = this.state.selectionIds.indexOf(object.id);
        const newSelection = this.state.selection.splice(selectionIdx, 1);
        const newSelectionIds = this.state.selectionIds.splice(selectionIdx, 1);
        this.setState({
            selection: newSelection,
            selectionIds: newSelectionIds,
        });
        object.unselect();
        this.emit(EVENT_TYPES.OBJECTS_SELECTION.CHANGED, this.state.selection);
    }

    addToSelection(object) {
        if (!(object instanceof Marker) && !(object instanceof Path)) {
            log('trying to add an unknow object to the selection');
            return;
        }
        if (this.state.selectionIds.indexOf(object.id) > -1) {
            return;
        }
        log('Adding object to selection: ', object);
        const newSelection = this.state.selection.slice(0); // clone
        newSelection.push(object);
        const newSelectionIds = this.state.selectionIds.slice(0);
        newSelectionIds.push(object.id);
        this.setState({
            selection: newSelection,
            selectionIds: newSelectionIds,
        });
        object.select();
        log('New selection', this.selection);
        this.emit(EVENT_TYPES.OBJECTS_SELECTION.CHANGED, this.state.selection);
    }

    setState(newState) {
        this.state = Object.freeze(Object.assign({}, this.state, newState));
        this.emit(EVENT_TYPES.STATE.CHANGE, this);
    }

    createAndAddLine(name, newLine, targetLayer) {
        if (!targetLayer) {
            targetLayer = this.selectedLayer;
        }
        const pathData = {
            mapProvider: this.mapProvider,
            layer: targetLayer,
            polyline: newLine,
            meta: {
                name: name || 'Path' + this.$pathCounter,
                layerId: targetLayer.id,
            },
        };
        this.$pathCounter++;
        const polyline = new Path(pathData);

        targetLayer.add(polyline);
        this.emit(EVENT_TYPES.PATH.CREATED, polyline, targetLayer);
        this.emit(EVENT_TYPES.LAYERS.CHANGED, this.layers);
        return polyline;
    }

    $stopMapEventPropagation = (evt) => {
        evt.originalEvent.stopPropagation();
        evt.originalEvent.preventDefault();
        this.mapProvider.stopEventPropagation(evt);
    };

    $markerDragStarted = (evt, dragMarker) => {
        for (let i = 0; i < this.selection.length; i++) {
            this.selection[i].setOriginalLatLng();
        }
        log('drag started', evt);
    };

    $markerDragEnded = (evt, dragMarker) => {
        for (let i = 0; i < this.selection.length; i++) {
            this.selection[i].clearOriginalLatLng();
        }
        log('drag ended', evt);
    };

    $markerDragging = (evt, dragMarker) => {
        log('drag ', evt, dragMarker, this.selection);
        const latDiff = evt.latlng.lat - evt.oldLatLng.lat;
        const lngDiff = evt.latlng.lng - evt.oldLatLng.lng;
        log('drag diff ', latDiff, lngDiff, 'selection items', this.selection.length);
        for (let i = 0; i < this.selection.length; i++) {
            if (this.selection[i].id === dragMarker.id) {
                continue;
            }
            log('moving', this.selection[i].id, 'ltLng:', this.selection[i].getLatLng());
            this.selection[i].moveToLatLng(
                this.selection[i].originalLatLng.lat + latDiff,
                this.selection[i].originalLatLng.lng + lngDiff,
            );
        }
    };

    $onMarkerClicked = (evt, markerInstance) => {
        if (this.mode === MODES.MOVE_OR_ROTATE_MODE && !markerInstance.selected) {
            // disable multiple selection
            this.addToSelection(markerInstance);
            // this.clearSelection();
            markerInstance.enableDrag(
                this.$markerDragStarted,
                this.$markerDragEnded,
                this.$markerDragging,
                markerInstance,
            );
            this.$stopMapEventPropagation(evt);
        }
    };

    createAndAddMarker(nativeMarker, targetLayer, data, notify = true) {
        if (!targetLayer) {
            targetLayer = this.selectedLayer;
        }
        this.$markerCounter++;

        const markerName = (data && data.name) || 'Marker' + this.$markerCounter;

        const marker = new Marker({
            layerObject: nativeMarker,
            mapProvider: this.mapProvider,
            layer: targetLayer,
            meta: {
                name: markerName,
                layerId: targetLayer.id,
                lat: nativeMarker.getLatLng().lat,
                lng: nativeMarker.getLatLng().lng,
            },
        });

        targetLayer.add(marker, notify);

        if (notify) {
            this.emit(EVENT_TYPES.MARKER.CREATED, marker, targetLayer);
            this.emit(EVENT_TYPES.LAYERS.CHANGED, this.layers);
        }
        nativeMarker.on('click', (evt) => this.$onMarkerClicked(evt, marker));

        this.setState({});
        return marker;
    }

    // $createMarker(newMarker, targetLayer, options) {
    //     return
    // }

    $createLayer(options) {
        return new Layer({
            mapProvider: this.mapProvider,
            meta: options.meta,
            id: options.id,
        });
    }

    $addLayer(layer, notify = true) {
        const { layers } = this.state;
        layers.push(layer);
        this.setState({
            layers,
        });
        this.mapProvider.addLayerToMap(layer.layerObject);
        if (notify) {
            this.emit(EVENT_TYPES.LAYERS.CHANGED, this.layers);
        }
    }

    // TODO: add target layer to support layers heirarchy
    /*
data needs to be another layer or marker object
*/
    createAndAddLayer(name = null) {
        const layer = this.$createLayer({
            meta: {
                name: name || `Layer ${this.$layerCounter}`,
            },
        });
        this.$layerCounter++;
        this.$addLayer(layer);
        return layer;
    }

    selectLayerById(layerId) {
        for (let i = 0; i < this.layers.length; i++) {
            if (this.state.layers[i].id === layerId) {
                this.setState({
                    selectedLayer: this.state.layers[i],
                    selectedLayerId: this.state.layers[i].id,
                });
                this.emit(EVENT_TYPES.LAYERS.SELECTION_CHANGED, this.selectedLayer);
                return;
            }
        }
        throw new Error('Layer not found');
    }

    selectLayer(layer) {
        this.selectLayerById(layer.id);
    }

    toJSON() {
        return {
            mode: this.state.mode,
            drawModeType: this.state.drawModeType,
            selectedLayerId: this.state.selectedLayerId,
            selectionIds: this.state.selectionIds,
            minZoom: this.state.minZoom,
            maxZoom: this.state.maxZoom,
            zoom: this.state.zoom,
            lat: this.state.lat,
            lng: this.state.lng,
            rightDrawer: {
                opened: this.state.rightDrawer.opened,
            },
            layers: this.state.layers.map((layer) => layer.toJSON()),
        };
    }

    showRightDrawer(show) {
        this.setState({
            rightDrawer: Object.assign({}, this.state.rightDrawer, { opened: show }),
        });
        this.emit(EVENT_TYPES.RIGHT_DRAWER.VISIBILITY_CHANGED, show);
    }

    loadGpx(data) {
        const gpxLayer = new L.GPX(data, { async: true }).on('loaded', (e) => {
            // this.mapProvider.addLayerToMap(gpxLayer);
            this.createAndAddLine(
                `GPX path ${this.$pathCounter}`,
                gpxLayer.getLayers()[0].getLayers()[0],
                this.selectedLayer,
            );
            this.mapProvider.fitBounds(e.target.getBounds());
        });
    }

    setMode(mode, modeOptions, updateState = true) {
        const newState = {
            mode,
        };
        if (mode === MODES.DRAW_MODE) {
            newState.drawModeType = modeOptions.drawModeType;
        }
        if (updateState) {
            this.setState(newState);
        }
        this.emit(EVENT_TYPES.MODE.CHANGE, {
            mode,
            modeOptions,
        });
        return newState;
    }
}

const createModel = function(data) {
    const intsance = new EditorModel(data);
    log(`Creating new model ${intsance.id}`);
    return intsance;
};

export { EditorModel, createModel, DEFAULT_LAT, DEFAULT_LNG, DEFAULT_ZOOM };
