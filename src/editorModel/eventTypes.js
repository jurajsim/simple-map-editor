export default {
    STATE: {
        CHANGE: 'map_editor_state_change',
    },
    MODE: {
        CHANGE: 'map_editor_mode_change',
    },
    MAP: {
        ZOOM_CHANGE: 'map_editor_map_zoom_change',
        LAT_LNG_CHANGE: 'map_editor_map_lat_lng_change',
    },
    LAYER: {
        CHILDREN_ADDED: 'map_editor_layer_children_added',
        VISIBILITY_CHANGED: 'map_editor_layer_visibility_changed',
    },
    LAYERS: {
        CHANGED: 'map_editor_layers_changed',
        SELECTION_CHANGED: 'map_editor_layers_selection_changed',
    },
    MARKER: {
        CREATED: 'map_editor_marker_created',
        VISIBILITY_CHANGED: 'map_editor_marker_visibility_changed',
    },
    PATH: {
        CREATED: 'map_editor_path_created',
    },
    RIGHT_DRAWER: {
        VISIBILITY_CHANGED: 'map_editor_right_drawer_visibility_changed',
    },
    OBJECTS_SELECTION: {
        CHANGED: 'map_editor_objects_selection_changed',
    },
};
