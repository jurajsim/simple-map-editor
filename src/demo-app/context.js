/**
 * inspiration: https://dev.to/oieduardorabelo/react-hooks-how-to-create-and-update-contextprovider-1f68
 */
import { createContext, useReducer } from 'react';

const AppContext = createContext();

const Actions = {
    TOGGLE_DRAWER_VISIBILITY: 'toggleDrawerVisibility',
};

const initialState = {
    drawerVisible: false,
};

const reducer = (state, action) => {
    switch (action.type) {
        case Actions.TOGGLE_DRAWER_VISIBILITY:
        return {
                ...state,
            drawerVisible: !state.drawerVisible,
            };
        default:
            throw new Error(`Unknown action ${action}`);
    }
};

const AppContextProvider = function(props) {
    // eslint-disable-next-line react/prop-types
    const { children } = props;
    const [state, dispatch] = useReducer(reducer, initialState);
    const toggleDrawer = () => {
        dispatch({ type: Actions.TOGGLE_DRAWER_VISIBILITY });
    };

    const value = {
        state,
        dispatch,
        toggleDrawer,
        get drawerVisible() {
            return state.drawerVisible;
        },
    };

    return <AppContext.Provider value={value}>{children}</AppContext.Provider>;
};

const AppContextConsumer = AppContext.Consumer;

export { AppContext, AppContextProvider, AppContextConsumer, Actions };
