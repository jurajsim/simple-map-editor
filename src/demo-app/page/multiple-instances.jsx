import Typography from '@material-ui/core/Typography';
import styles from '@/demo-app/index';
import Editor from '@/editor-components/material-io/editor';
import React from 'react';
import Debug from 'debug';

const debug = Debug('multiple-instances-page');

const onModelChanged = (id, jsonModel) => {
    localStorage.setItem(`editor-model-${id}`, JSON.stringify(jsonModel));
    debug('Local storage "editor-model" updated with new model', jsonModel);
};

const MultipleInstancesPage = function() {
    let storedData;
    let storedData2;
    try {
        storedData = JSON.parse(localStorage.getItem('editor-model-first'));
    } catch (e) {
        console.error('Unable to load editor 1 data from local storage', e);
    }

    try {
        storedData2 = JSON.parse(localStorage.getItem('editor-model-second'));
    } catch (e) {
        console.error('Unable to load editor 2 data from local storage', e);
    }

    return (
        <React.Fragment>
            <Typography variant="h4">Instance 1</Typography>
            <div className={styles.flexContainer}>
                <div className={styles.flexCenteredItem}>
                    {/* default max auto height */}
                    <Editor
                        height={768}
                        id="first"
                        data={storedData}
                        onChange={(model) => onModelChanged('first', model)}
                    />
                </div>
            </div>
            <br />
            <br />
            <br />
            <Typography variant="h4">Instance 2</Typography>
            <div className={styles.flexContainer}>
                <div className={styles.flexCenteredItem}>
                    {/* default max auto height */}
                    <Editor
                        height={768}
                        id="second"
                        data={storedData2}
                        onChange={(model) => onModelChanged('second', model)}
                    />
                </div>
            </div>
        </React.Fragment>
    );
};

export default MultipleInstancesPage;
