import { AppContextConsumer, AppContextProvider } from '@/demo-app/context';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/inbox';
import 'core-js/stable';
import React from 'react';
import { hot } from 'react-hot-loader';
import { BrowserRouter as Router, Route, withRouter } from 'react-router-dom';
import 'regenerator-runtime/runtime';
import styled from 'styled-components';
import MultipleInstancesPage from './page/multiple-instances';
import TopToolbar from '@/demo-app/component/top-toolbar';
import { AppBar } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';

const PageContent = styled.div`
    margin-top: 64px;
`;

const CenteredDiv = styled.div`
    width: 100%;
    height: calc(100vh - 128px);
    padding: 1em;
    display: flex;
    align-items: center;
    justify-content: center;
`;

const Home = function Home() {
    return <CenteredDiv>Hello, select some example!</CenteredDiv>;
};

const Spacer = styled.div`
    flex-grow: 1;
`;

const LeftMenu = withRouter(({ history }) => {
    const openPage = (to, toggleDrawer) => {
        history.push(to);
        toggleDrawer();
    };

    return (
        <AppContextConsumer>
            {({ drawerVisible, toggleDrawer }) => (
                <Drawer open={drawerVisible} onClose={toggleDrawer}>
                    <AppBar position="static">
                        <Toolbar>
                            <Spacer />
                            <IconButton onClick={toggleDrawer} color="inherit">
                                <CloseIcon />
                            </IconButton>
                        </Toolbar>
                    </AppBar>
                    <List>
                        <ListItem button onClick={() => openPage('/', toggleDrawer)} key="home">
                            <ListItemIcon>
                                <InboxIcon />
                            </ListItemIcon>
                            <ListItemText primary="Home" />
                        </ListItem>
                        <ListItem
                            button
                            onClick={() => openPage('/multiple-instances', toggleDrawer)}
                            key="multiple-instances"
                        >
                            <ListItemIcon>
                                <InboxIcon />
                            </ListItemIcon>
                            <ListItemText primary="Multiple instances" />
                        </ListItem>
                    </List>
                </Drawer>
            )}
        </AppContextConsumer>
    );
});

const Index = () => {
    return (
        <AppContextProvider>
            <Router>
                <TopToolbar />
                <LeftMenu />
                <PageContent>
                    <Route exact path="/" component={Home} />
                    <Route path="/multiple-instances" component={MultipleInstancesPage} />
                </PageContent>
            </Router>
        </AppContextProvider>
    );
};

export default hot(module)(Index);
