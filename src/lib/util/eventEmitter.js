import debug from 'debug';
import EventEmitter from 'events';

const log = debug('ro:emitter');

class Emitter extends EventEmitter {
    on() {
        log(`Registering event handler `, arguments);
        super.on(...arguments);
    }

    off() {
        log(`Unregistering event handler `, arguments);
        super.off(...arguments);
    }

    emit() {
        log(`Emitting event handler `, arguments);
        super.emit(...arguments);
    }
}

export default Emitter;
