export default {
    create(options) {
        return {
            url: '//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            options: {
                attribution:
                    '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
                minZoom: options.minZoom || 8,
                maxNativeZoom: 19,
                maxZoom: options.maxZoom || 25,
            },
        };
    },
};
