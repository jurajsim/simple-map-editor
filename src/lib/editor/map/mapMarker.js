import L from 'leaflet';
import 'leaflet-draw-drag';
import FlowerIcon from './icons/plantIcon';

const ICON_WIDTH = 64;
const ICON_HEIGHT = 64;

export const Marker = L.Marker.extend({
    options: {
        data: null,
    },
});

// TODO: verify if there is no other way than overloading the whole method?
export const DrawMarker = L.Draw.Marker.extend({
    _createMarker(latlng) {
        return new Marker(latlng, {
            icon: this.options.icon,
            zIndexOffset: this.options.zIndexOffset,
            transform: true,
            draggable: true,
        });
    },
});

export const MarkerIcon = new FlowerIcon({
    iconSize: [ICON_WIDTH, ICON_HEIGHT],
    iconAnchor: [ICON_WIDTH / 2, ICON_HEIGHT],
});
