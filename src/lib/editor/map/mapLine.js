import L from 'leaflet';

const customPath = L.Polyline.extend({
    options: {
        data: null,
        weight: 6,
    },
});

export const DrawLine = L.Draw.Polyline.extend({
    Poly: customPath,
    options: {
        shapeOptions: {
            transform: true,
            draggable: true,
        },
    },
});
