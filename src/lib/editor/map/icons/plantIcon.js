import ReactDOMServer from 'react-dom/server';
import L from 'leaflet';
import Flower from './flower.svg';
import './plantIcon.css';
import SvgImage from './marker.svg';

const Marker = (props) => {
    return (<div>
              <div className="marker">
                <SvgImage width={64} height={64}></SvgImage>
                <div className="marker-bg"></div>
              </div>
      <div className="flower">
        <Flower width={32} height={32}></Flower>
      </div>
    </div>);
}

export default L.DivIcon.extend({
    // createIcon: function (oldIcon) {
    //     return <Flower>;
    // },
    // createIcon: (oldIcon) => {
    //     return React.cloneElement(Flower);
    // },
    options: {
        className: 'leaflet-flower-icon',
        html: ReactDOMServer.renderToString(<Marker></Marker>),
//         html: `
//                <div class="marker">
//                    <svg viewBox="${svgImage.viewBox}">
//                         <use xlink:href="#${svgImage.id}"/>
//                    </svg>
//                    <div class="marker-bg"></div>
//                </div>
//
//                <div class="flower">
//                     <svg viewBox="${flower.viewBox}">
//                         <use xlink:href="#${flower.id}"/>
//                     </svg>
//                </div>
// `,
    },
});
