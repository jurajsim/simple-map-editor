export default {
    LAYER: {
        CREATE_AND_ADD: 'map_editor_create_and_add_layer',
    },
    DIALOG: {
        SHOW_MODEL: 'map_editor_show_model_dialog',
    },
    MAP: {
        READY: 'map_ready',
    },
};
