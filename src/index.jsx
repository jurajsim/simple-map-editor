import React from 'react';
import ReactDOM from 'react-dom';
import DemoApp from './demo-app/index.jsx';

ReactDOM.render(<DemoApp />, document.getElementById('app'));
