import * as PropTypes from 'prop-types';
import React, { lazy, Suspense, useEffect } from 'react';
import ModelEvents from '@/editorModel/eventTypes';
import TopBar from '@/components/topToolbar';
import StatusBar from '@/components/statusbar';
import RightDrawer from '@/components/rightDrawer';
import SourceDialog from '@/components/dialog/source';
import { createModel } from '@/editorModel/editorModel';
import uuid from 'uuid';
import styles from './editor.scss';
import ZoomControls from './map-controls/zoom-controls';

import {
    UiVisibilityContextConsumer,
    UiVisibilityContextProvider,
    Actions,
} from '../ui-visibility-context';

const MapCanvas = lazy(() => import('@/components/mapCanvas'));

export default function Editor({ onChange, id, data, height }) {
    const model = createModel(data);
    if (!id) {
        // eslint-disable-next-line no-param-reassign
        id = uuid();
    }

    useEffect(() => {
        model.on(ModelEvents.STATE.CHANGE, onChange);
        return () => {
            model.off(ModelEvents.STATE.CHANGE, onChange);
        };
    });

    return (
        <UiVisibilityContextProvider>
            <div className={styles.smeMainDiv}>
                <TopBar editorModel={model} />
                <div style={{ height: height - 24 - 48 }}>
                    <RightDrawer editorModel={model} height={height} />
                    <Suspense fallback={<div>Loading...</div>}>
                        <MapCanvas id={id} editorModel={model} height={height - 24 - 48} />
                    </Suspense>
                </div>
                <ZoomControls model={model} />
                <StatusBar editorModel={model} />
                <UiVisibilityContextConsumer>
                    {({ state: visibilityState, dispatch: visibilityDispatch }) => (
                        <SourceDialog
                            open={visibilityState.codeViewVisible}
                            onClose={() =>
                                visibilityDispatch({ type: Actions.TOGGLE_CODE_VIEW_VISIBILITY })
                            }
                            {...{ model, height }}
                        />
                    )}
                </UiVisibilityContextConsumer>
            </div>
        </UiVisibilityContextProvider>
    );
}

Editor.defaultProps = {
    height: 600,
    id: uuid(),
    data: null,
    onChange: () => {
        console.warn('Editor model updated, but no change handler finction defined!');
    },
};

Editor.propTypes = {
    height: PropTypes.number,
    data: PropTypes.object,
    id: PropTypes.string,
    onChange: PropTypes.func,
};
