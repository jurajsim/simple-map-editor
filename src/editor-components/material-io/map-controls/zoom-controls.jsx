import * as PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import Fab from '@material-ui/core/Fab';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import styles from './zoom-controls.scss';
import EDITOR_EVENT_TYPES from '@/lib/editor/eventTypes';
import {Model} from '@/editorModel';

export default function ZoomControls({ model }) {
    let mapProviderRef;

    useEffect(() => {
        const updateMapProvider = ({ mapProvider }) => {
            mapProviderRef = mapProvider;
        };

        model.on(EDITOR_EVENT_TYPES.MAP.READY, updateMapProvider);
        return () => {
            model.off(EDITOR_EVENT_TYPES.MAP.READY, updateMapProvider);
        };
    });

    return (
        <div className={styles.smeZoomControlsPanel}>
            <Fab
                color="inherit"
                size="medium"
                aria-label="Zoom in"
                className={styles.zoomIn}
                onClick={() => mapProviderRef.zoomIn()}>
                <ZoomInIcon />
            </Fab>
            <Fab
                color="inherit"
                size="medium"
                aria-label="Zoom out"
                className={styles.zoomOut}
                onClick={() => mapProviderRef.zoomOut()}>
                <ZoomOutIcon />
            </Fab>
        </div>
    );
}

ZoomControls.defaultProps = {
    model: null,
};

ZoomControls.propTypes = {
    model: PropTypes.instanceOf(Model),
};
