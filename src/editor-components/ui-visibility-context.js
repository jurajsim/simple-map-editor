/**
 * inspiration: https://dev.to/oieduardorabelo/react-hooks-how-to-create-and-update-contextprovider-1f68
 */
import { createContext, useReducer } from 'react';

const UiVisibilityContext = createContext();

const Actions = {
    TOGGLE_RIGHT_DRAWER_VISIBILITY: 'toggleRightDrawerVisibility',
    TOGGLE_CODE_VIEW_VISIBILITY: 'toggleCodeViewVisibility',
};

const initialState = {
    rightDrawerVisible: false,
    codeViewVisible: false,
};

const reducer = (state, action) => {
    switch (action.type) {
        case Actions.TOGGLE_RIGHT_DRAWER_VISIBILITY:
            return {
                ...state,
            rightDrawerVisible: !state.rightDrawerVisible,
        };
        case Actions.TOGGLE_CODE_VIEW_VISIBILITY:
            return {
                ...state,
                codeViewVisible: !state.codeViewVisible,
            };
        default:
            throw new Error(`Unknown action ${action}`);
    }
};

const UiVisibilityContextProvider = function(props) {
    const { children } = props;
    const [state, dispatch] = useReducer(reducer, initialState);
    const value = { state, dispatch };

    return <UiVisibilityContext.Provider value={value}>{children}</UiVisibilityContext.Provider>;
};

const UiVisibilityContextConsumer = UiVisibilityContext.Consumer;

export { UiVisibilityContext, UiVisibilityContextProvider, UiVisibilityContextConsumer, Actions };
