import React from "react";

export default props => {
  const {color, title, handleClick, icon} = props;
  const newProps = {
    color,
  };
  return (
    <StyledIcon
      onClick={handleClick}
      title={title}
      aria-label={title}
    >
      {React.cloneElement(icon, newProps)}
    </StyledIcon>
  );
};
