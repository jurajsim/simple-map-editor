import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import CloseIcon from './baseline-close-24px.svg';
import EDITOR_MODEL_EVENT_TYPES from '../editorModel/eventTypes';
import LayersListWidget from './rightDrawer/layersListWidget';
import MarkerOptions from '@/components/rightDrawer/marker-options';
import TYPES from '@/editorModel/objects/types';
import { Model } from '@/editorModel';
import { UiVisibilityContextConsumer, Actions } from '@/editor-components/ui-visibility-context';

const DRAWER_WIDTH = '16em';

const IconButton = styled.div`
    margin: 1em;
`;

const Drawer = styled.div`
    position: absolute;
    top: 0;
    right: 0;
    transition: transform 0.15s ease-out;
    transform: translate3d(${(props) => (props.open ? 0 : DRAWER_WIDTH)}, 0, 0);
    z-index: 1500;
    height: calc(${(props) => props.height}px);
    opacity: 0.9;
    width: calc(${DRAWER_WIDTH} - 0.5em - 0.5em);
    background-color: white;
    padding: 0;
`;

const AppBar = styled.div`
  position: relative;
  background-color: #3f51b5;
  top: 0;
  right: 0;
  height: 64px);
  display: flex;
  flex-direction: row;
  align-items: center;
  color: white;
`;

const AppBarTitle = styled.h6`
    margin: 0 0 0 1em;
`;

class RightDrawer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            opened: false,
            showMartkerProperties: false,
            layers: this.props.editorModel.layers,
            selectedLayer: this.props.editorModel.selectlayer,
            selectedMarker: null,
        };
    }

    onLayersChanged = (newLayers) => {
        this.setState({
            layers: this.props.editorModel.layers,
        });
    };

    onLayersSelectionChanged = (selectedlayer) => {
        this.setState({
            selectedLayer: selectedlayer,
        });
    };

    onDrawerVisibilitychnaged = (opened) => {
        this.setState({
            opened,
        });
    };

    close = () => {
        this.props.editorModel.showRightDrawer(false);
    };

    onSelectionChanged = (selection) => {
        if (selection.length !== 1) {
            this.setState({
                selectedMarker: null,
                showMartkerProperties: false,
            });
            return;
        }
        if (selection.length === 1 && selection[0].type === TYPES.MARKER) {
            this.setState({
                showMartkerProperties: true,
                selectedMarker: selection[0],
            });
        }
    };

    componentDidMount() {
        this.props.editorModel.on(
            EDITOR_MODEL_EVENT_TYPES.RIGHT_DRAWER.VISIBILITY_CHANGED,
            this.onDrawerVisibilitychnaged,
        );
        this.props.editorModel.on(EDITOR_MODEL_EVENT_TYPES.LAYERS.CHANGED, this.onLayersChanged);
        this.props.editorModel.on(
            EDITOR_MODEL_EVENT_TYPES.OBJECTS_SELECTION.CHANGED,
            this.onSelectionChanged,
        );
        this.props.editorModel.on(
            EDITOR_MODEL_EVENT_TYPES.LAYERS.SELECTION_CHANGED,
            this.onLayersSelectionChanged,
        );
    }

    render() {
        const { height, editorModel } = this.props;
        const { showMartkerProperties, selectedMarker } = this.state;
        return (
            <UiVisibilityContextConsumer>
                {({ state: visibilityState, dispatch: visibilityDispatch }) => (
                    <Drawer open={visibilityState.rightDrawerVisible} height={height}>
                        <AppBar position="static" color="primary">
                            <AppBarTitle>Properties</AppBarTitle>
                            <div style={{ flexGrow: 1 }} />
                            <IconButton
                                onClick={() => {
                                    visibilityDispatch({ type: Actions.TOGGLE_RIGHT_DRAWER_VISIBILITY });
                                }}>
                                <CloseIcon />
                            </IconButton>
                        </AppBar>
                        <LayersListWidget
                            editorModel={editorModel}
                            layers={editorModel.layers}
                            selectedLayer={editorModel.selectedLayer}
                        />
                        <hr />
                        {showMartkerProperties && selectedMarker && (
                            <MarkerOptions marker={selectedMarker} editorModel={editorModel} />
                        )}
                    </Drawer>
                )}
            </UiVisibilityContextConsumer>
        );
    }
}

export default RightDrawer;

RightDrawer.propTypes = {
    editorModel: PropTypes.instanceOf(Model).isRequired,
};
