import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Collapse from "../../../node_modules/@material-ui/core/Collapse/Collapse";
import IconButton from "../../../node_modules/@material-ui/core/es/IconButton/IconButton";
import ListItem from "../../../node_modules/@material-ui/core/es/ListItem/ListItem";
import ListItemSecondaryAction from "../../../node_modules/@material-ui/core/es/ListItemSecondaryAction/ListItemSecondaryAction";
import ListItemText from "../../../node_modules/@material-ui/core/es/ListItemText/ListItemText";
import List from "../../../node_modules/@material-ui/core/List/List";
import ListItemIcon from "../../../node_modules/@material-ui/core/ListItemIcon/ListItemIcon";
import TrashIcon from "../../../node_modules/@material-ui/icons/Delete";
import VisibleIcon from "../../../node_modules/@material-ui/icons/Visibility";
import InvisibleIcon from "../../../node_modules/@material-ui/icons/VisibilityOff";
import EDITOR_MODEL_EVENT_TYPES from "../../editorModel/eventTypes";
import { EditorModel } from "../../editorModel";
import LayersListWidget from "./layersListWidget";

const StylesListItem = styled(ListItem)`
  min-width: 14em;
  padding-left: 0.5em !important;
  background-color: ${props =>
    props.selected ? "#e4e6ff" : "#ffffff"} !important;
`;

const StyledIconButton = styled(IconButton)`
  width: 24px !important;
  height: 24px !important;
`;

export default class MarkerWidget extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: this.props.marker.visible
    };
  }

  componentWillUnmount() {
    this.props.marker.off(
      EDITOR_MODEL_EVENT_TYPES.MARKER.VISIBILITY_CHANGED,
      this.onVisibilityChanged
    );
  }

  componentDidMount() {
    this.props.marker.on(
      EDITOR_MODEL_EVENT_TYPES.MARKER.VISIBILITY_CHANGED,
      this.onVisibilityChanged
    );
  }

  toggleLayerVisibility = evt => {
    if (this.props.marker.visible) {
      this.props.marker.hide();
    } else {
      this.props.marker.show();
    }
    evt.stopPropagation();
  };

  onVisibilityChanged = visible => {
    this.setState({ visible });
  };

  render() {
    return (
      <StylesListItem selected={false} button dense>
        <ListItemText>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <IconButton
            size="small"
            onClick={evt => this.toggleLayerVisibility(evt)}
            aria-label="Visibility"
          >
            {this.state.visible ? <VisibleIcon /> : <InvisibleIcon />}
          </IconButton>
          {this.props.marker.meta.name}
        </ListItemText>
        <ListItemSecondaryAction>
          <IconButton size="small" aria-label="Delete">
            <TrashIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </StylesListItem>
    );
  }
}

MarkerWidget.propTypes = {
  marker: PropTypes.object.isRequired
};
