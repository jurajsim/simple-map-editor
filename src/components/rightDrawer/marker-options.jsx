import React, { useState } from 'react';
import { TextField } from '@material-ui/core';
import { Model } from '@/editorModel';

export default (props) => {
    const [title, setTitle] = useState(props.marker.getTitle());

    const { marker } = props;
    const { editorModel } = props;

    const handleChange = (evt) => {
        setTitle(evt.target.value);
        marker.setTitle(evt.target.value);
        editorModel.setState({}); //TODO: add something like update method...
    };

    return (
        <div>
            <div>Marker options</div>
            <br />
            <TextField label="Title" value={title} onChange={handleChange} />
        </div>
    );
};
