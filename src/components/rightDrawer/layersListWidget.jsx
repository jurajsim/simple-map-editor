import React from "react";
import List from "@material-ui/core/es/List/List";
import styled from "styled-components";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/es/Grid/Grid";
import Button from "@material-ui/core/es/Button/Button";
import { Model } from "../../editorModel";
import EDITOR_MODEL_EVENT_TYPES from "../../editorModel/eventTypes";
import EDITOR_EVENT_TYPES from "../../lib/editor/eventTypes";
import LayerWidget from "./layerWidget";
import Debug from 'debug';

const debug = Debug('layers-widget');

const Widget = styled(Grid)`
  padding: 0.5em;
`;

const StyledList = styled(List)`
  max-height: 500px;
  overflow-y: auto;
`;

export default class LayersListWidget extends React.Component {
  selectLayer = layer => {
    this.props.editorModel.selectLayer(layer);
  };

  addLayer() {
    this.props.editorModel.emit(EDITOR_EVENT_TYPES.LAYER.CREATE_AND_ADD);
  }

  componentWillUnmount() {
    this.props.editorModel.off(
      EDITOR_MODEL_EVENT_TYPES.LAYERS.CHANGED,
      this.onLayersChanged
    );
  }

  componentDidMount() {
    this.props.editorModel.on(
      EDITOR_MODEL_EVENT_TYPES.LAYERS.CHANGED,
      this.onLayersChanged
    );
  }

  onLayersChanged = layers => {
    debug("Layers", layers);
  };

  render() {
    return (
      <Widget container direction="column">
        <Grid item>
          <StyledList>
            {this.props.layers.map((layer, key) => (
              <LayerWidget
                key={key}
                layer={layer}
                onSelect={this.selectLayer}
                selectedLayer={this.props.editorModel.selectedLayer}
              />
            ))}
          </StyledList>
        </Grid>
        <Grid item>
          <Button onClick={this.addLayer} variant="contained" color="primary">
            Add layer
          </Button>
        </Grid>
      </Widget>
    );
  }
}

LayersListWidget.propTypes = {
  editorModel: PropTypes.instanceOf(Model).isRequired,
  layers: PropTypes.array.isRequired
};
