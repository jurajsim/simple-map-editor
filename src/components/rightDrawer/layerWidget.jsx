import debug from "debug";
import PropTypes from "prop-types";
import React from "react";
import styled from "styled-components";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import Collapse from "../../../node_modules/@material-ui/core/Collapse/Collapse";
import IconButton from "../../../node_modules/@material-ui/core/es/IconButton/IconButton";
import ListItem from "../../../node_modules/@material-ui/core/es/ListItem/ListItem";
import ListItemSecondaryAction  from "../../../node_modules/@material-ui/core/es/ListItemSecondaryAction/ListItemSecondaryAction";
import ListItemText             from "../../../node_modules/@material-ui/core/es/ListItemText/ListItemText";
import List                     from "../../../node_modules/@material-ui/core/List/List";
import TrashIcon                from "../../../node_modules/@material-ui/icons/Delete";
import VisibleIcon              from "../../../node_modules/@material-ui/icons/Visibility";
import InvisibleIcon            from "../../../node_modules/@material-ui/icons/VisibilityOff";
import EDITOR_MODEL_EVENT_TYPES from "../../editorModel/eventTypes";
import Icon                     from "../icons/icon";
import MarkerWidget             from "./markerWidget";

const log = debug("right-drawer-layer-widget");

const StylesListItem = styled(ListItem)`
  min-width: 14em;
  padding-left: 0.5em !important;
  background-color: ${props =>
    props.selected ? "#e4e6ff" : "#ffffff"} !important;
`;

export default class LayerWidget extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      children: this.props.layer.children,
      open: false,
      visible: this.props.layer.visible
    };
  }

  componentWillUnmount() {
    this.props.layer.off(
      EDITOR_MODEL_EVENT_TYPES.LAYER.CHILDREN_ADDED,
      this.onChildrenChanged
    );
    this.props.layer.off(
      EDITOR_MODEL_EVENT_TYPES.LAYER.VISIBILITY_CHANGED,
      this.onVisibilityChanged
    );
  }

  componentDidMount() {
    this.props.layer.on(
      EDITOR_MODEL_EVENT_TYPES.LAYER.CHILDREN_ADDED,
      this.onChildrenChanged
    );
    this.props.layer.on(
      EDITOR_MODEL_EVENT_TYPES.LAYER.VISIBILITY_CHANGED,
      this.onVisibilityChanged
    );
  }

  onVisibilityChanged = visible => {
    this.setState({ visible });
  };

  onChildrenChanged = children => {
    this.setState({ children });
  };

  toggleExpand = () => {
    this.setState({ open: !this.state.open });
  };

  toggleLayerVisibility = (evt, layer) => {
    log("Toggle visibility of ", layer);
    if (this.props.layer.visible) {
      this.props.layer.hide();
    } else {
      this.props.layer.show();
    }
    evt.stopPropagation();
  };

  render() {
    return (
      <React.Fragment>
        <StylesListItem
          selected={
            this.props.selectedLayer &&
            this.props.layer.id === this.props.selectedLayer.id
          }
          button
          dense
          onClick={() => {
            this.props.onSelect(this.props.layer);
          }}
        >
          <ListItemText>
            <IconButton
              size="small"
              onClick={evt => this.toggleLayerVisibility(evt, this.props.layer)}
              aria-label="Visibility"
            >
              {this.state.visible ? <VisibleIcon /> : <InvisibleIcon />}
            </IconButton>
            {this.props.layer.meta.name}
          </ListItemText>
          <ListItemSecondaryAction>
            {this.state.children.length > 0 ? (
              <IconButton
                size="small"
                aria-label="Expand"
                onClick={this.toggleExpand}
              >
                {this.state.open ? <ExpandLess/> : <ExpandMore/>}
              </IconButton>
            ) : (
              <div style={{ width: 24 }} />
            )}
            <IconButton size="small" aria-label="Delete">
              <TrashIcon />
            </IconButton>
          </ListItemSecondaryAction>
        </StylesListItem>
        <Collapse in={this.state.open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {this.state.children.length &&
              this.state.children.map((item, idx) => (
                <MarkerWidget key={idx} marker={item} />
              ))}
          </List>
        </Collapse>
      </React.Fragment>
    );
  }
}

LayerWidget.propTypes = {
  selectedLayer: PropTypes.object,
  onSelect: PropTypes.func.isRequired,
  layer: PropTypes.object.isRequired
};
