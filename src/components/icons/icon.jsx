import React from 'react'
// import './../../resources/icons/icons.svg'

const DEFAULT_WIDTH = 16;
const DEFAULT_HEIGHT = 16;

const Icon = (props) => (
    <svg style={{width: props.width || DEFAULT_WIDTH, height: props.height || DEFAULT_HEIGHT}} className={`icon icon-${props.name}`}>
        <use xlinkHref={`#icons_${props.name}`} />
    </svg>
);

export default Icon
