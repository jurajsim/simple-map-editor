import React from "react";
import debug from "debug";
import styled from "styled-components";
import PropTypes from "prop-types";
import EDITOR_MODEL_EVENT_TYPES from "../editorModel/eventTypes";
import MODES from "../editorModel/modes";
import { Model } from "../editorModel";
import Marker from "../editorModel/objects/marker";
import Path from "../editorModel/objects/path";

const log = debug("statusBar");

const StatusbarItem = styled.div`
  margin-left: 8px;
  padding: 4px;
`;

const StatusBarRow = styled.div`
  background-color: #f5f5f5;
  width: 100%;
  height: 24px;
  font-size: 12px;
  display: flex;
  flex-direction: row;
`;

class StatusBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mode: this.getModeString(this.props.editorModel.mode),
      zoom: this.props.editorModel.zoom,
      selectedLayer: this.getLayerName(this.props.editorModel.selectedLayer),
      selection: this.getSelection(this.props.editorModel.selection)
    };
  }

  getModeString = mode => {
    if (mode === MODES.EDIT_MODE) {
      return "ED";
    }
    if (mode === MODES.DRAW_MODE) {
      return "DR";
    }
    if (mode === MODES.MOVE_OR_ROTATE_MODE) {
      return "MR";
    }
    return "UN";
  };

  getLayerName = layer => {
    if (!layer) {
      return "none";
    }
    return layer.name;
  };

  onEditorModelChange = modeInfo => {
    this.setState({
      mode: this.getModeString(this.props.editorModel.mode),
      zoom: this.props.editorModel.zoom,
      selectedLayer: this.getLayerName(this.props.editorModel.selectedLayer),
      selection: this.getSelection(this.props.editorModel.selection)
    });
  };

  getSelection(selection) {
    if (selection.length > 1) {
      return `${selection.length} objects`;
    }
    if (selection.length === 1) {
      log("Selected object", selection[0]);
      if (selection[0] instanceof Marker) {
        return `${selection.length} marker`;
      } else if (selection[0] instanceof Path) {
        log("Got poly");
        return `${selection.length} polyline`;
      }
      return `${selection.length} object`;
    }
    return "none";
  }

  componentDidMount() {
    this.props.editorModel.on(
      EDITOR_MODEL_EVENT_TYPES.STATE.CHANGE,
      this.onEditorModelChange
    );
  }

  render() {
    return (
      <StatusBarRow container spacing={0} alignItems="center">
        <div>
          <StatusbarItem>Mode: {this.state.mode}</StatusbarItem>
        </div>
        <div>
          <StatusbarItem>Zoom: {this.state.zoom}</StatusbarItem>
        </div>
        <div>
          <StatusbarItem>Layer: {this.state.selectedLayer}</StatusbarItem>
        </div>
        <div>
          <StatusbarItem>Selection: {this.state.selection}</StatusbarItem>
        </div>
        <div style={{flex: 'auto'}}></div>
      </StatusBarRow>
    );
  }
}

export default StatusBar

StatusBar.propTypes = {
  editorModel: PropTypes.instanceOf(Model).isRequired
};
