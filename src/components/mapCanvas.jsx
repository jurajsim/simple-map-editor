import React from 'react';
import L from 'leaflet';
import 'leaflet-draw-drag';
import 'leaflet-path-transform';
import 'leaflet/dist/leaflet.css';
import 'leaflet-draw/dist/leaflet.draw.css';
import 'leaflet-gpx';
import './map-canvas.css';
import styled from 'styled-components';
import debug from 'debug';
import uuid from 'uuid';
import BaseLayer from '../lib/editor/map/layers/baseLayer';
import EDITOR_MODEL_EVENT_TYPES from '../editorModel/eventTypes';
import EDITOR_EVENT_TYPES from '../lib/editor/eventTypes';
import MODES from '../editorModel/modes';
import TYPES from '../editorModel/objects/types';
import { DrawMarker, MarkerIcon } from '../lib/editor/map/mapMarker';
import { DrawLine } from '../lib/editor/map/mapLine';
import { Model } from '@/editorModel';

const MapDiv = styled.div`
    height: ${(props) => props.height}px;
    width: 100%;
    top: 48px;
    left: 0;
    position: absolute;
    overflow: hidden;
`;

const log = debug('map-canvas');

export default class MapCanvas extends React.Component {
    constructor(props) {
        super(props);
        this.map = null;
        this.drawObject = null;
        this.mapSettings = {
            zoomControl: false,
        };
        this.state = {
            id: props.id || uuid(),
            zoom: this.props.editorModel.zoom,
        };
    }

    onMapZoomChanged = (evt) => {
        this.props.editorModel.setZoom(this.map.getZoom());
    };

    $stopMapEventPropagation = (evt) => {
        evt.originalEvent.stopPropagation();
        evt.originalEvent.preventDefault();
        L.DomEvent.stopPropagation(evt);
    };

    $onPolylineClicked = (evt, polylineInstance) => {
        if (
            this.props.editorModel.mode === MODES.MOVE_OR_ROTATE_MODE &&
            !polylineInstance.selected
        ) {
            this.props.editorModel.clearSelection();
            polylineInstance.enableDrag();
            polylineInstance.enableTransform();
            this.props.editorModel.addToSelection(polylineInstance);
            this.$stopMapEventPropagation(evt);
        } else if (this.props.editorModel.mode === MODES.EDIT_MODE && !polylineInstance.selected) {
            log('enable line edit mode', polylineInstance);
            this.props.editorModel.clearSelection();
            this.props.editorModel.addToSelection(polylineInstance);
            polylineInstance.polylineObject.options.editing = {};
            this.drawObject = new L.Edit.Poly(polylineInstance.polylineObject);
            this.drawObject.enable();
            this.$stopMapEventPropagation(evt);
        }
    };

    $onLineCreated = (darwingObject) => {
        const newPolyline = darwingObject.layer;

        const polylineInstance = this.props.editorModel.createAndAddLine(
            this.map,
            newPolyline,
            this.props.editorModel.selectedLayer,
        );
        newPolyline.data = polylineInstance; // ??needed
        newPolyline.on('click', (evt) => this.$onPolylineClicked(evt, polylineInstance));

        this.props.editorModel.selectedLayer.layerObject.addLayer(newPolyline);
        this.props.editorModel.setMode(MODES.DRAW_MODE, {
            drawModeType: TYPES.LINE,
        });
    };

    $onMarkerCreated = (darwingObject) => {
        const newMarker = darwingObject.layer;

        this.props.editorModel.createAndAddMarker(newMarker, this.props.editorModel.selectedLayer);
    };

    $onDrawingCreated = (darwingObject) => {
        if (darwingObject.layerType === TYPES.MARKER) {
            return this.$onMarkerCreated(darwingObject);
        }
        if (darwingObject.layerType === TYPES.LINE) {
            return this.$onLineCreated(darwingObject);
        }
        console.warn('Unknown drawing object created', darwingObject);
    };

    $onDrawingCancelled = () => {
        if (this.props.editorModel.mode === MODES.DRAW_MODE) {
            this.props.editorModel.setMode(MODES.MOVE_OR_ROTATE_MODE);
        }
    };

    $onMapClicked = (evt) => {
        log('map clicked', evt);
        // if (evt.originalEvent.srcElement.tagName === 'path') {
        //     log('clicked on path, exit');
        //     return;
        // }
        if (this.props.editorModel.selection.length > 0) {
            this.props.editorModel.clearSelection();
        }
        if (this.props.editorModel.mode === MODES.EDIT_MODE && this.drawObject) {
            this.drawObject.disable();
            this.drawObject = null;
        }
    };

    onMapMoved = () => {
        const latLng = this.map.getCenter();
        this.props.editorModel.setLatLng(latLng.lat, latLng.lng);
    };

    componentDidMount() {
        const baseLayer = BaseLayer.create({
            minZoom: this.props.editorModel.minZoom,
            maxZoom: this.props.editorModel.maxZoom,
        });
        this.map = L.map(this.state.id, this.mapSettings).setView(
            [this.props.editorModel.lat, this.props.editorModel.lng],
            this.state.zoom,
        );
        L.tileLayer(baseLayer.url, baseLayer.options).addTo(this.map);
        this.map.on('zoomend', this.onMapZoomChanged);
        this.map.on('moveend', this.onMapMoved);
        this.map.on('draw:created', this.$onDrawingCreated);
        this.map.on('draw:canceled', this.$onDrawingCancelled);
        this.map.on('click', this.$onMapClicked);
        this.props.editorModel.on(EDITOR_MODEL_EVENT_TYPES.MODE.CHANGE, this.onEditorModeChange);
        this.props.editorModel.on(
            EDITOR_EVENT_TYPES.LAYER.CREATE_AND_ADD,
            this.onCreateAndAddLayer,
        );

        // TODO: should this be here???
        if (this.props.editorModel.mode) {
            this.onEditorModeChange({
                mode: this.props.editorModel.mode,
                modeOptions: { drawModeType: this.props.editorModel.drawModeType },
            });
        }
        this.props.editorModel.syncWithMap(this.map);
    }

    onCreateAndAddLayer = () => {
        return this.props.editorModel.createAndAddLayer();
    };

    onEditorModeChange = (evtOptions) => {
        log('map mode change', evtOptions);
        if (this.drawObject) {
            this.drawObject.disable();
            this.drawObject = null;
        }
        if (
            evtOptions.mode === MODES.DRAW_MODE &&
            evtOptions.modeOptions.drawModeType === TYPES.MARKER
        ) {
            this.drawObject = new DrawMarker(this.map, {
                icon: MarkerIcon,
            });
            this.drawObject.enable();
        } else if (
            evtOptions.mode === MODES.DRAW_MODE &&
            evtOptions.modeOptions.drawModeType === TYPES.LINE
        ) {
            this.drawObject = new DrawLine(this.map);
            this.drawObject.enable();
        }
    };

    render() {
        return <MapDiv height={this.props.height} id={this.state.id} />;
    }
}
