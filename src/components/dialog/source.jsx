import React from 'react';
import styled from 'styled-components';

const Dialog = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    background-color: white;
    display: grid;
    width: 100%;
    height: ${(props) => props.height}px;
    grid-template-rows: 40px auto;
    z-index: 9999;
`;

const DialogToolbar = styled.div`
    grid-row: 1;
    display: flex;
    flex-direction: row;
`;

const CloseLink = styled.div`
    text-decoration: underline;
`;

const DialogBody = styled.div`
    grid-row: 2;
    overflow: auto;
`;

export default function(props) {
    const { open, height, onClose, model } = props;
    if (!open) {
        return '';
    }
    return (
        <Dialog height={height}>
            <DialogToolbar>
                <div>Model Source</div>
                <div style={{ flex: 'auto' }}></div>
                <CloseLink onClick={onClose}>close</CloseLink>
            </DialogToolbar>
            <DialogBody>
                <pre>{JSON.stringify(model.toJSON(), null, 4)}</pre>
            </DialogBody>
        </Dialog>
    );
}
