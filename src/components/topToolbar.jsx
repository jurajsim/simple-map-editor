import uuid from 'uuid';
import React from 'react';
import AppBar from '@material-ui/core/es/AppBar/AppBar';
import MoreVerticalIcon from '@material-ui/icons/MoreVert';
import ModelIcon from '@material-ui/icons/Code';
import debug from 'debug';
import MoveOrRotateIcon from '@material-ui/icons/ZoomOutMap';
import EditIcon from '@material-ui/icons/Edit';
import MarkerIcon from '@material-ui/icons/AddLocation';
import IconListAdd from '@material-ui/icons/MergeType';
import LineIcon from '@material-ui/icons/LinearScale';
import MODES from '../editorModel/modes';
import OBJECT_TYPES from '../editorModel/objects/types';
import EDITOR_MODEL_EVENT_TYPES from '../editorModel/eventTypes';
import EDITOR_EVENT_TYPES from '../lib/editor/eventTypes';
import { StyledToolbar, StyledIcon } from './topToolbar/styled';
import { UiVisibilityContextConsumer, Actions } from '@/editor-components/ui-visibility-context';

const log = debug('top-menu');

const SPACER = 'spacer';

export default class TopToolbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            icons: this.getIcons(),
            ready: false,
        };

        this.fileUploader = React.createRef();
    }

    componentDidMount() {
        this.setState({
            ready: true,
        });
        this.props.editorModel.on(EDITOR_MODEL_EVENT_TYPES.MODE.CHANGE, this.onModeChange);
    }

    getIcons() {
        return [
            {
                // key: 'button-1',
                title: 'Move or rotate',
                icon: <MoveOrRotateIcon />,
                color: MODES.isMoveOrRotateMode(this.props.editorModel) ? 'primary' : 'default',
                onClick: this.setMoveOrRotateMode,
            },
            {
                // key: 'button-2',
                title: 'Edit',
                icon: <EditIcon />,
                color: MODES.isEditMode(this.props.editorModel) ? 'primary' : 'default',
                onClick: this.setEditMode,
            },
            {
                // key: 'button-3',
                title: 'Draw marker',
                icon: <MarkerIcon />,
                color: MODES.isDrawingMarker(this.props.editorModel) ? 'primary' : 'default',
                onClick: this.setDrawMarkerMode,
            },
            {
                // key: 'button-4',
                title: 'Draw line',
                icon: <LineIcon />,
                color: MODES.isDrawingLine(this.props.editorModel) ? 'primary' : 'default',
                onClick: this.setDrawLineMode,
            },
            {
                // key: 'button-5',
                title: 'Import gpx',
                icon: <IconListAdd />,
                color: 'default',
                onClick: this.importGpxFile,
            },
            SPACER,
        ];
    }

    importGpxFile = () => {
        console.log(this.fileUploader);
        this.fileUploader.current.click();
    };

    setDrawLineMode = () => {
        this.props.editorModel.setMode(MODES.DRAW_MODE, {
            drawModeType: OBJECT_TYPES.LINE,
        });
    };

    setDrawMarkerMode = () => {
        this.props.editorModel.setMode(MODES.DRAW_MODE, {
            drawModeType: OBJECT_TYPES.MARKER,
        });
    };

    setMoveOrRotateMode = () => {
        this.props.editorModel.setMode(MODES.MOVE_OR_ROTATE_MODE);
    };

    setEditMode = () => {
        this.props.editorModel.setMode(MODES.EDIT_MODE);
    };

    onModeChange = (evtOptions) => {
        const { ready } = this.state;
        log(`Mode changed to ${evtOptions.newMode} with options: `, evtOptions.newModeOptions);
        if (ready) {
            this.setState({
                icons: this.getIcons(),
            });
        }
    };

    showModelDialog = () => {
        this.props.editorModel.emit(EDITOR_EVENT_TYPES.DIALOG.SHOW_MODEL, true);
    };

    onGpxSelected = (file) => {
        // let file = evt.target.file;

        const reader = new FileReader();

        reader.addEventListener('load', () => {
            this.props.editorModel.loadGpx(reader.result);
        });

        reader.readAsDataURL(file);
    };

    render() {
        return (
            <AppBar position="static" color="default">
                <StyledToolbar variant="dense">
                    {this.state.icons.map((item) => {
                        if (item === SPACER) {
                            return (
                                <div key={`topToolbarSpacer_${uuid()}`} style={{ flexGrow: 1 }} />
                            );
                        }
                        return (
                            <StyledIcon
                                key={`button_${uuid()}`}
                                {...item}
                                theme={{
                                    mixins: { toolbar: { minHeight: 64 } },
                                    spacing: { unit: 'px' },
                                }}
                            >
                                {React.cloneElement(item.icon)}
                            </StyledIcon>
                        );
                    })}
                    <UiVisibilityContextConsumer>
                        {({ dispatch: visibilityDispatch }) => (
                            <React.Fragment>
                                <StyledIcon
                                    key="show_code_view_toggle"
                                    title="Show code view"
                                    color="primary"
                                    onClick={() => {
                                        visibilityDispatch({
                                            type: Actions.TOGGLE_CODE_VIEW_VISIBILITY,
                                        });
                                    }}
                                    theme={{
                                        mixins: { toolbar: { minHeight: 64 } },
                                        spacing: { unit: 'px' },
                                    }}
                                >
                                    <ModelIcon />
                                </StyledIcon>
                                <StyledIcon
                                    key="show_layers_toggle"
                                    title="Show layers"
                                    color="primary"
                                    onClick={() => {
                                        visibilityDispatch({
                                            type: Actions.TOGGLE_RIGHT_DRAWER_VISIBILITY,
                                        });
                                    }}
                                    theme={{
                                        mixins: { toolbar: { minHeight: 64 } },
                                        spacing: { unit: 'px' },
                                    }}
                                >
                                    <MoreVerticalIcon />
                                </StyledIcon>
                            </React.Fragment>
                        )}
                    </UiVisibilityContextConsumer>
                </StyledToolbar>
                <input
                    id="file-input"
                    onChange={(e) => this.onGpxSelected(e.target.files[0])}
                    type="file"
                    ref={this.fileUploader}
                    style={{ display: 'none' }}
                />
            </AppBar>
        );
    }
}
